- [**Description**](#description)
- [**Technology**](#technology)
- [**Year**](#year)
- [**Diagram případů užití**](#diagram-pripadu-uziti)
- [**Kontextový diagram**](#kontextovy-diagram)
- [**Datová analýza**](#datova-analyza)
  - [**Lineární zápis seznamu typů entit a jejich atributů**](#linearni-zapis-seznamu-typu-entit-a-jejich-atributu)
    - [Nahrazení vztahů M:N](#nahrazeni-vztahu-m-n)
  - [**Grafický tvar konceptuálního modelu (ERD, UML)**](#graficky-tvar-konceptualniho-modelu-erd-uml)
  - [**Grafický tvar logického modelu**](#graficky-tvar-logickeho-modelu)
  - [**Grafický tvar relačního datového modelu**](#graficky-tvar-relacniho-datoveho-modelu)
  - [**Úplné tabulky atributů (tj. datový slovník) a integritní omezení**](#uplne-tabulky-atributu-tj-datovy-slovnik-a-integritni-omezeni)
- [**Funkční závislosti a normální formy**](#funkcni-zavislosti-a-normalni-formy)
  - [Sestavení množiny funkčních závislostí](#sestaveni-mnoziny-funkcnich-zavislosti)
  - [**Sestavení relačního datového modelu v BCNF**](#sestaveni-relacniho-datoveho-modelu-v-bcnf)
- [**Porovnání původního relačního modelu získaného z konceptuálního modelu a modelu v BCNF**](#porovnani-puvodniho-relacniho-modelu-ziskaneho-z-konceptualniho-modelu-a-modelu-v-bcnf)
- [**Docs**](#docs)
- [**Presentations**](#presentations)
- [**SQL scripts**](#sql-scripts)


### **Description**
[zadání pdf](/README/zadani.pdf)  

**PROČ**  
Je potřeba vytvořit komplexní informační systém městské hromadné dopravy, který bude sloužit jak pro vnitřní, tak pro vnější účely.  
**K ČEMU**  
Tento informační systém bude sloužit k vyhledávání spojů městské hromadné dopravy podle různých kritérií, jakými jsou například typ vozidla, jestli vozidlo umožňuje bezbariérový přístup, vyhledávání jen v zastávkách umožňujících bezbariérový přístup do vozidla a zároveň bude sloužit k evidenci řidičů a k práci s jejich směnami.   
**KDO**  
Se systémem bude moci pracovat každý, kdo si chce vyhledat nějaký spoj městské hromadné dopravy, řidiči vozidel městské hromadné dopravy, správce systému a také zaměstnavatel řidičů.  
**VSTUPY**  
U řidičů evidujeme rodné číslo, jméno, příjmení, bydliště, telefonní číslo, pohlaví, heslo a plat.
U směn evidujeme typ – tedy název směny, v kolik začíná, v kolik končí a kolik je to dohromady hodin.
U vozidel evidujeme typ – jestli se jedná o tramvaj, autobus, metro, trolejbus nebo vlak, dále pak jestli je vozidlo s bariérovým přístupem, značku, max. počet cestujících a řidiče, který se o vozidlo stará.
Dále evidujeme, jací řidiči jezdí s jakými vozidly a od kolika, do kolika hodin jezdí vozidlo určitou linku.
U zastávek evidujeme identifikační číslo zastávky, její název a možnost bezbariérového přístup.
Dále pak v kolik hodin zastavuje na konkrétní zastávce která linka.
U zón evidujeme číslo zóny a pak jestli se jedná o městskou, nebo mimoměstskou oblast.
U linek evidujeme číslo linky a typ linky (vozidla jakého typu po ní jezdí – tramvaj/autobus/metro/trolejbus/vlak)  
**VÝSTUPY**   
Výstupem pro správce nebo zaměstnavatele budou všichni řidiči, všechna vozidla, jaký řidič se stará o jaké vozidlo, jací řidiči řídí jaká vozidla, všechny zastávky, linky, zóny.
Pro nepřihlášeného uživatele bude výstupem spojení dle zadaných kritérií včetně všech zastávek, které leží mezi zadanou vstupní a výstupní zastávkou, všechny odjezdy ze zadané zastávky, všechny spoje z dané zastávky.
Pro řidiče se zobrazí rozpis směn, všechny detaily o vozidle, o které se stará, všechny směny včetně jejich detailů.  
**FUNKCE**  
Pokud přidáme nové vozidlo, musíme mu také přidělit řidiče, který se o toto vozidlo bude starat.
Po přidání zastávky se musí zadat, ve které zóně leží a do které linky bude zastávka patřit.
Každý řidič patří do jednoho rozpisu směn, kde se směny „točí“.
Pro každé vyhledávání vyhledá k zastávce odjezdu trasu (všechny navštívené zastávky) k zastávce příjezdu.  
**OKOLÍ**  
Kdokoli, kdo se k aplikaci dostane, může v aplikaci vyhledávat spoje. 
Pracovníci městské hromadné dopravy pak používají aplikaci pro vnitřní účely.  

---
### **Technology**
SQL

---
### **Year**
2012

---
### **Diagram případů užití**
![](./README/usecase2.png)

---
### **Kontextový diagram**
![](./README/kontextDiagram.png)

---
### **Datová analýza**
#### **Lineární zápis seznamu typů entit a jejich atributů**
**Entity**  
Ridic (rc, jmeno, prijmenu, bydliste, tel, pohlavi, heslo, plat)  
Vozidlo (idVozidla, tyo, znacka, maxPocet, barierove, rc, rc_ridi)  
Linka (cisloLinky, typLinky)  
Zastavka (idZastavky, nazev, barierova, cisloZony)  
Zona (cisloZony, mesto)  
Smena (typSmeny, začátek, konec, hodin)  

**Vztahy**  
stara_se_o (Ridic, Vozdilo) 1:1  
ridi(Ridic, Vozidlo) N:M  
jezdi (Vozdilo, Linka) N:M  
se sklada (Linka, Zastavka) N:M  
obsahuje (Zona, Zastavka) 1:N  
chodi (Ridic, RozpisSmen) N:M  

##### Nahrazení vztahů M:N
**Entity**  
Ridic (rc, jmeno, prijmenu, bydliste, tel, pohlavi, heslo, plat)  
Vozidlo (idVozidla, tyo, znacka, maxPocet, barierove, rc)  
Linka (cisloLinky, typLinky)  
Zastavka (idZastavky, nazev, barierova, cisloZony)  
Zona (cisloZony, mesto)  
Smena (typSmeny, začátek, konec, hodin)  
VozidloLinka (od, do, idVozidla, cisloLinky)  
ZastavkaLinkaCas (cas, cisloLinky, idZastavky)  
RozpisSmen (datum, jmenoSmeny, rc, typSmeny)  
RidicVozidlo (rc, idVozidla)  

**Vztahy**  
stara_se_o (Ridic, Vozdilo) 1:1  
ridi_Ridic (Ridic, RidicVozidlo) 1:N  
ridi_Vozidlo (Vozidlo, RidicVozidlo) 1:N   
jezdi (Vozdilo, VozidloLinka) 1:N  
ma (Linka, VozidloLinka) 1:N  
se sklada (Linka, ZastavkaLinkaCas) 1:N  
zastavuje (Zastavka, zastavkaLinkaCas) 1:N  
obsahuje (Zona, Zastavka) 1:N  
chodi (Ridic, RozpisSmen) 1:N  
je v (Smena, RozpisSmen) 1:N  

#### **Grafický tvar konceptuálního modelu (ERD, UML)**
![](./README/er_koncept.png)

#### **Grafický tvar logického modelu**
![](./README/Logical%20model.png)

#### **Grafický tvar relačního datového modelu**
![](./README/er_mhd.png)

#### **Úplné tabulky atributů (tj. datový slovník) a integritní omezení**
![](./README/datovy_slovnik.png)
...  
NN = not null, N = null, PK = primární klíč, FK = cizí klíč, PFK = cizí klíč je součást primárního klíče

---
### **Funkční závislosti a normální formy**
#### Sestavení množiny funkčních závislostí
**Universální schéma**  
U (rc, jméno, prijmeni, bydliste, tel, pohlavi, heslo, plat, idVozidla, typVozidla, znacka, maxPocet, barierove, od, do, cisloLinky, typLinky, cas, IdZastavky, nazev, barierova, cisloZony, mesto, datum, jmenoSmeny, typSmeny, zacatek, konec, hodin)

**přejmenování pro rychlejší a přehlednější zpracování:**  
U (rc, jm, pr, bydl, tel, pohl, hes, plat, idV, typV, zn, maxP, barV, od, do, cisL, typL, cas, idZ, naz, barZ, cisZ, met, dat, jmS, typS, zac, kon, hod)

**Množina všech funkčních závislostí**  
F = {  
    rc -> jm pr bydl tel pohl hes plat  
    idV -> typV zn maxP barV rc  
    idV cisL -> od do  
    cisL -> typL  
    cisL idZ -> cas  
    idZ -> naz barZ cisZ  
    cisZ -> met  
    rc typS -> dat, jmS  
    typS -> zac, kon, hod  
}  

**Klíč univerzálního schématu**  
rc 	           += { rc jm pr bydl tel pohl hes plat }  
idV 	      += { idV typV zn maxP barV rc jm pr bydl tel pohl hes plat }  
idV cisL	+= { idV typV zn maxP barV rc jm pr bydl tel pohl hes plat cisL typL od do } cisL	       += { cisL typL }  
cisL idZ    += { cisL typL idZ naz barZ cisZ met cas }  
idZ 	      += { idZ naz barZ cisZ met }  
cisZ	      += { cisZ met }  
rc typS	    += { rc jm pr bydl tel pohl hes plat typS zac kon hod dat jmS }  
typS 	     += { typS zac kon hod }  

Klíč universálního schématu **K = {idV cisL idZ typS}**

Ve funkčních závislostech se žádné redundantní závislosti ani atributy nevyskytují, tudíž F = Fmin.

#### **Sestavení relačního datového modelu v BCNF**
**Rozklad pomocí dekompozice**  
![](./README/Rozklad_dekompozice.jpg)

**Rozklad pomocí syntézy**  
![](./README/Rozklad_synteza.jpg)

---
### **Porovnání původního relačního modelu získaného z konceptuálního modelu a modelu v BCNF**
Oba rozklady se od konceptuálního modelu výrazně neliší. Liší se jen tím, že tabulka RidicVozidlo, která reprezentuje m:n vztah mezi řidiči a vozidly, je identifikována pomocí klíče univerzálního schématu – tedy atributy idVozidla, cisloLinky, idZastavky, TypSmeny.

---
### **Docs**  
[MHD docx](/README/MHD.docx)  
[MHD pdf](/README/MHD.pdf)  

[MHD SQL dotazy pdf](/README/MHD%20-%20dotazy.pdf)  
[MHD SQL dotazy docx](/README/MHD%20-%20dotazy.docx)  

---
### **Presentations**  
[prezentace1 pdf](/README/prezentace1.pdf)  
[prezentace1 ppt](/README/prezentace1.ppt)  
[prezentace1 pptx](/README/prezentace1.pptx)  

[prezentace2 pdf](/README/prezentace2.pdf)  
[prezentace2 ppt](/README/prezentace2.ppt)  
[prezentace2 pptx](/README/prezentace2.pptx)  

---
### **SQL scripts**  
[SQL script schema](/README/Skript_schema.sql)  
[SQL script data](/README/Skript_data.sql)  
[SQL script query](/README/Skript_query.sql)  