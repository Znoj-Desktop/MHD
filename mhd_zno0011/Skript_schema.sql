﻿/*
Created: 18. 12. 2012
Modified: 19. 12. 2012
Model: Logical model
Database: MS SQL Server 2008
*/


-- Table Vozidlo

CREATE TABLE [Vozidlo]
(
 [idVozidla] Int NOT NULL,
 [typVozidla] Varchar(30) NOT NULL,
 [znacka] Varchar(30) NOT NULL,
 [maxPocet] Int NOT NULL,
 [barierove] Bit NULL,
 [rc] Bigint NOT NULL
)
go

-- Add keys for table Vozidlo

ALTER TABLE [Vozidlo] ADD CONSTRAINT [Unique_Identifier1] PRIMARY KEY ([idVozidla])
go

-- Table VozidloLinka

CREATE TABLE [VozidloLinka]
(
 [od] Time NOT NULL,
 [do] Time NOT NULL,
 [idVozidla] Int NOT NULL,
 [cisloLinky] Int NOT NULL
)
go

-- Add keys for table VozidloLinka

ALTER TABLE [VozidloLinka] ADD CONSTRAINT [Unique_Identifier2] PRIMARY KEY ([idVozidla],[cisloLinky])
go

-- Table Linka

CREATE TABLE [Linka]
(
 [cisloLinky] Int NOT NULL,
 [typLinky] Varchar(30) NOT NULL
)
go

-- Add keys for table Linka

ALTER TABLE [Linka] ADD CONSTRAINT [Unique_Identifier3] PRIMARY KEY ([cisloLinky])
go

-- Table Zastavka

CREATE TABLE [Zastavka]
(
 [idZastavky] Int NOT NULL,
 [nazev] Varchar(30) NOT NULL,
 [barierova] Bit NULL,
 [cisloZony] Int NOT NULL
)
go

-- Add keys for table Zastavka

ALTER TABLE [Zastavka] ADD CONSTRAINT [Unique_Identifier4] PRIMARY KEY ([idZastavky])
go

-- Table Zona

CREATE TABLE [Zona]
(
 [cisloZony] Int NOT NULL,
 [mesto] Bit NOT NULL
)
go

-- Add keys for table Zona

ALTER TABLE [Zona] ADD CONSTRAINT [Unique_Identifier5] PRIMARY KEY ([cisloZony])
go

-- Table Ridic

CREATE TABLE [Ridic]
(
 [rc] Bigint NOT NULL,
 [jmeno] Varchar(30) NOT NULL,
 [prijmeni] Varchar(30) NOT NULL,
 [bydliste] Varchar(30) NOT NULL,
 [tel] Bigint NOT NULL,
 [pohlavi] Varchar(4) NULL,
 [heslo] Varchar(20) NOT NULL,
 [plat] Int NOT NULL
)
go

-- Add keys for table Ridic

ALTER TABLE [Ridic] ADD CONSTRAINT [Unique_Identifier6] PRIMARY KEY ([rc])
go

-- Table Smena

CREATE TABLE [Smena]
(
 [typSmeny] Varchar(30) NOT NULL,
 [zacatek] Varchar(5) NOT NULL,
 [konec] Varchar(5) NOT NULL,
 [hodin] Int NULL
)
go

-- Add keys for table Smena

ALTER TABLE [Smena] ADD CONSTRAINT [Unique_Identifier7] PRIMARY KEY ([typSmeny])
go

-- Table RozpisSmen

CREATE TABLE [RozpisSmen]
(
 [datum] Date NOT NULL,
 [jmenoSmeny] Char(256) NOT NULL,
 [rc] Bigint NOT NULL,
 [typSmeny] Varchar(30) NOT NULL
)
go

-- Add keys for table RozpisSmen

ALTER TABLE [RozpisSmen] ADD CONSTRAINT [Unique_Identifier8] PRIMARY KEY ([rc],[typSmeny])
go

-- Table ZastavkaLinkaCas

CREATE TABLE [ZastavkaLinkaCas]
(
 [cas] Time NOT NULL,
 [cisloLinky] Int NOT NULL,
 [idZastavky] Int NOT NULL
)
go

-- Add keys for table ZastavkaLinkaCas

ALTER TABLE [ZastavkaLinkaCas] ADD CONSTRAINT [Unique_Identifier9] PRIMARY KEY ([cisloLinky],[idZastavky])
go

-- Table RidicVozidlo

CREATE TABLE [RidicVozidlo]
(
 [rc] Bigint NOT NULL,
 [idVozidla] Int NOT NULL
)
go

-- Create relationships section ------------------------------------------------- 

ALTER TABLE [VozidloLinka] ADD CONSTRAINT [jezdi] FOREIGN KEY ([idVozidla]) REFERENCES [Vozidlo] ([idVozidla])
go

ALTER TABLE [VozidloLinka] ADD CONSTRAINT [ma] FOREIGN KEY ([cisloLinky]) REFERENCES [Linka] ([cisloLinky])
go

ALTER TABLE [Zastavka] ADD CONSTRAINT [obsahuje] FOREIGN KEY ([cisloZony]) REFERENCES [Zona] ([cisloZony])
go

ALTER TABLE [Vozidlo] ADD CONSTRAINT [stara_se_o] FOREIGN KEY ([rc]) REFERENCES [Ridic] ([rc])
go

ALTER TABLE [RozpisSmen] ADD CONSTRAINT [chodi] FOREIGN KEY ([rc]) REFERENCES [Ridic] ([rc])
go

ALTER TABLE [RozpisSmen] ADD CONSTRAINT [je_v] FOREIGN KEY ([typSmeny]) REFERENCES [Smena] ([typSmeny])
go

ALTER TABLE [ZastavkaLinkaCas] ADD CONSTRAINT [se_sklada] FOREIGN KEY ([cisloLinky]) REFERENCES [Linka] ([cisloLinky])
go

ALTER TABLE [ZastavkaLinkaCas] ADD CONSTRAINT [zastavuje] FOREIGN KEY ([idZastavky]) REFERENCES [Zastavka] ([idZastavky])
go

ALTER TABLE [RidicVozidlo] ADD CONSTRAINT [ridi_Ridic] FOREIGN KEY ([rc]) REFERENCES [Ridic] ([rc])
go

ALTER TABLE [RidicVozidlo] ADD CONSTRAINT [ridi_Vozidlo] FOREIGN KEY ([idVozidla]) REFERENCES [Vozidlo] ([idVozidla])
go




